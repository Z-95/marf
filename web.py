import logging
import util

logger = logging.getLogger(__name__)
anidb_base_url = "http://api.anidb.net:9001/httpapi?client=marf&clientver=1&protover=1"
titles_url = "http://anidb.net/api/anime-titles.xml.gz"
mappings_url = "https://raw.githubusercontent.com/ScudLee/anime-lists/master/anime-list-master.xml"


def get_mylist(user, password):
    logger.debug("Getting mylist for user {} from web".format(user))
    result = util.get_data_from_url("{}&request=mylistsummary&user={}&pass={}".format(anidb_base_url, user, password))
    if "<error" in result:
        logger.critical("Error received from anidb: {}".format(result))
        raise Exception("Error received from anidb")
    return result


def get_anime(aid):
    logger.debug("Getting anime aid {} from web".format(aid))
    result = util.get_data_from_url("{}&request=anime&aid={}".format(anidb_base_url, aid))
    if "<error" in result:
        logger.critical("Error received from anidb: {}".format(result))
        raise Exception("Error received from anidb")
    return result


def get_titles():
    logger.debug("Getting titles from web")
    return util.get_data_from_url(titles_url)


def get_mappings():
    logger.debug("Getting mappings from web")
    return util.get_data_from_url(mappings_url)
