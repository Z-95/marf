import gzip
import time
import urllib.request

url_request_rate_limit = 5000
last_request_time = 0


def current_time():
    return int(round(time.time() * 1000))


def get_data_from_url(url):
    global last_request_time
    if last_request_time + url_request_rate_limit > current_time():
        time.sleep(url_request_rate_limit / 1000)
    response = urllib.request.urlopen(url)
    if response.getheader("Content-Encoding") == "gzip":
        data = gzip.decompress(response.read())
    else:
        data = response.read()
    last_request_time = current_time()
    return data.decode(encoding='UTF-8')
