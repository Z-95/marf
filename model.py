import enum
import logging
from xml.etree import ElementTree

logger = logging.getLogger(__name__)


class MyList:
    def __init__(self):
        self.uid = None
        self.items = {}

    def __init__(self, raw):
        tree = ElementTree.ElementTree(ElementTree.fromstring(raw))
        mylist_summary_element = tree.getroot()
        self.uid = int(mylist_summary_element.get("uid"))
        self.items = {}
        for mylist_item_element in mylist_summary_element.findall("mylistitem"):
            item = MyListItem()
            aid = int(mylist_item_element.get("aid"))
            item.state = MyListItemState(int(mylist_item_element.get("state")))
            item.episodes = int(mylist_item_element.findtext("episodecount", "0"))
            item.episodes_seen = int(mylist_item_element.findtext("seencount", "0"))
            item.specials = int(mylist_item_element.findtext("specialcount", "0"))
            item.specials_seen = int(mylist_item_element.findtext("seenspecialcount", "0"))
            if item.episodes_seen == item.episodes and item.specials_seen == item.specials:
                item.watched = True
            else:
                item.watched = False
            self.items[aid] = item


class MyListItem:
    def __init__(self):
        self.state = None
        self.episodes = None
        self.specials = None
        self.episodes_seen = None
        self.specials_seen = None
        self.watched = None


class MyListItemState(enum.Enum):
    mixed = -1
    unknown = 1
    internal = 2
    external = 3
    deleted = 4


class Anime:
    def __init__(self):
        self.id = None
        self.restricted = None
        self.type = None
        self.start_date = None
        self.episode_count = None
        self.titles = []
        self.related_aids = []
        self.episodes = []
        self.special_count = None
        self.resources = {}
        self.picture = None

    def __init__(self, raw):
        tree = ElementTree.ElementTree(ElementTree.fromstring(raw))
        anime_element = tree.getroot()
        self.id = int(anime_element.get("id"))
        restricted = anime_element.get("restricted")
        if restricted == "true":
            self.restricted = True
        else:
            self.restricted = False
        self.type = anime_element.findtext("type")
        self.start_date = anime_element.findtext("startdate")
        self.episode_count = int(anime_element.findtext("episodecount"))
        self.titles = []
        for title_element in anime_element.find("titles").findall("title"):
            title = AnimeTitle()
            title.language = title_element.get("{http://www.w3.org/XML/1998/namespace}lang")
            title.type = AnimeTitleType[title_element.get("type")]
            title.title = title_element.text
            self.titles.append(title)
        self.related_aids = []
        related_element = anime_element.find("relatedanime")
        if related_element is not None:
            for related_anime_element in related_element.findall("anime"):
                self.related_aids.append(int(related_anime_element.get("id")))
        self.episodes = []
        self.special_count = 0
        episodes_element = anime_element.find("episodes")
        if episodes_element is not None:
            for episode_element in episodes_element.findall("episode"):
                episode = AnimeEpisode()
                episode.id = int(episode_element.get("id"))
                episode.type = AnimeEpisodeType(int(episode_element.find("epno").get("type")))
                if episode.type == AnimeEpisodeType.special:
                    self.special_count += 1
                episode.number = episode_element.findtext("epno")
                for episode_title_element in episode_element.findall("title"):
                    episode_title = AnimeEpisodeTitle()
                    episode_title.language = episode_title_element.get("{http://www.w3.org/XML/1998/namespace}lang")
                    episode_title.title = episode_title_element.text
                    episode.titles.append(episode_title)
                self.episodes.append(episode)
        self.resources = {}
        resources_element = anime_element.find("resources")
        if resources_element is not None:
            for resource_element in resources_element.findall("resource"):
                resource_type = AnimeResourceType(int(resource_element.get("type")))
                items = []
                for external_entity_element in resource_element.findall("externalentity"):
                    identifier_element = external_entity_element.find("identifier")
                    url_element = external_entity_element.find("url")
                    if identifier_element is not None:
                        items.append(identifier_element.text)
                    if url_element is not None:
                        items.append(url_element.text)
                self.resources[resource_type] = items
        self.picture = anime_element.findtext("picture")

    def nice_title(self):
        english_title = None
        main_title = None
        for title in self.titles:
            if title.language == "en" and title.type == AnimeTitleType.official:
                english_title = title.title
            if title.type == AnimeTitleType.main:
                main_title = title.title
        if english_title is None:
            english_title = "**" + main_title
        return english_title.replace("`", "'")


class AnimeResourceType(enum.Enum):
    ann_id = 1
    mal_id = 2
    animenfo_id = 3
    official_url = 4
    official_english_url = 5
    english_wikipedia = 6
    japanese_wikipedia = 7
    cal_syoboi_id = 8
    allcinema_id = 9
    anison_id = 10
    lain_gr_id = 11
    vndb_id = 14
    marumegane_id = 15
    animemorial_id = 16
    tv_animation_museum_id = 17
    korean_wikipedia = 19
    chinese_wikipedia = 20
    official_facebook = 22
    official_twitter = 23
    official_youtube = 26
    crunchyroll = 28
    media_art_db = 31


class AnimeEpisode:
    def __init__(self):
        self.id = None
        self.type = None
        self.number = None
        self.titles = []


class AnimeEpisodeTitle:
    def __init__(self):
        self.language = None
        self.title = None


class AnimeEpisodeType(enum.Enum):
    regular = 1
    special = 2
    credit = 3
    trailer = 4
    parody = 5
    other = 6


class AnimeMapping:
    def __init__(self):
        self.aid = None
        self.tvdbid = None
        self.default_tvdb_season = None
        self.episode_offset = None
        self.tmdbid = None
        self.imdbids = []
        self.mappings = []
        self.before = None

    def to_html(self):
        output = ""
        if self.tvdbid:
            try:
                output += "<a href=\"http://thetvdb.com/?tab=series&id={}\">TVDB: {}</a> "\
                    .format(int(self.tvdbid), int(self.tvdbid))
            except ValueError:
                output += "TVDB: {} ".format(self.tvdbid)
        if len(self.imdbids) > 0:
            for imdbid in self.imdbids:
                try:
                    output += "<a href=\"http://www.imdb.com/title/tt{}\">IMDB: tt{}</a> "\
                        .format(int(imdbid[2:]), int(imdbid[2:]))
                except ValueError:
                    output += "IMDB: {} ".format(imdbid)
        if self.tmdbid:
            output += "<a href=\"https://www.themoviedb.org/movie/{}\">TMDB: {}</a> " \
                .format(self.tmdbid, self.tmdbid)
        return output.strip()


class AnimeMappingMapping:
    def __init__(self):
        self.anidb_season = None
        self.tvdb_season = None
        self.start = None
        self.end = None
        self.offset = None
        self.data = None


class AnimeTitle:
    def __init__(self):
        self.language = None
        self.type = None
        self.title = None


class AnimeTitleType(enum.Enum):
    main = 1
    synonym = 2
    short = 3
    official = 4
