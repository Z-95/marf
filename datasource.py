import logging
import os
from xml.etree import ElementTree

import cache
import model
import util
import web

logger = logging.getLogger(__name__)
anime_expire_days = 30
mylist_expire_days = 1
mappings_expire_days = 5
titles_expire_days = 5
anime_memory_cache = {}
anime_mappings = {}
anime_titles = {}


def get_mylist(user, password):
    user_dtg = cache.get_user_dtg(user)
    if user_dtg > 0 and user_dtg > util.current_time() - mylist_expire_days * 86400000:
        logger.debug("Getting mylist for {} from cache".format(user))
        raw_mylist = cache.get_user_data(user)
    else:
        logger.debug("Getting mylist for {} from web".format(user))
        raw_mylist = web.get_mylist(user, password)
        cache.set_user(user, raw_mylist)
    mylist = model.MyList(raw_mylist)
    return mylist


def get_anime(aid):
    global anime_memory_cache
    if aid in anime_memory_cache:
        return anime_memory_cache[aid]
    anime_dtg = cache.get_anime_dtg(aid)
    if anime_dtg > 0 and anime_dtg > util.current_time() - anime_expire_days * 86400000:
        logger.debug("Getting anime {} from cache".format(aid))
        raw_anime = cache.get_anime_data(aid)
    else:
        logger.debug("Getting anime {} from web".format(aid))
        raw_anime = web.get_anime(aid)
        cache.set_anime(aid, raw_anime)
    anime = model.Anime(raw_anime)
    anime_memory_cache[aid] = anime
    return anime


def dump_anime_to_dir(output_dir):
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    data = cache.get_all_anime_data()
    for aid, data in data.items():
        output = os.path.join(output_dir, "{}.xml".format(aid))
        with open(output, "w", encoding="utf8") as output_file:
            output_file.write(data)


def load_anime_mappings():
    global anime_mappings
    mappings_dtg = cache.get_mappings_dtg()
    if mappings_dtg > 0 and mappings_dtg > util.current_time() - mappings_expire_days * 86400000:
        logger.debug("Getting anime mappings from cache")
        raw_mappings = cache.get_mappings_data()
    else:
        logger.debug("Getting anime mappings from web")
        raw_mappings = web.get_mappings()
        cache.set_mappings(raw_mappings)
    tree = ElementTree.ElementTree(ElementTree.fromstring(raw_mappings))
    for anime_element in tree.getroot().findall("anime"):
        mapping = model.AnimeMapping()
        mapping.aid = int(anime_element.get("anidbid"))
        mapping.tvdbid = anime_element.get("tvdbid")
        mapping.default_tvdb_season = anime_element.get("defaulttvdbseason")
        mapping.episode_offset = anime_element.get("episodeoffset")
        mapping.tmdbid = anime_element.get("tmdbid")
        imdbids = anime_element.get("imdbid")
        if imdbids:
            split_imdbids = imdbids.split(",")
            for imdbid in split_imdbids:
                mapping.imdbids.append(imdbid)
        mapping.before = anime_element.findtext("before")
        mapping_list_element = anime_element.find("mapping-list")
        if mapping_list_element is not None:
            for mapping_element in mapping_list_element.findall("mapping"):
                mapping_mapping = model.AnimeMappingMapping()
                mapping_mapping.anidb_season = mapping_element.get("anidbseason")
                mapping_mapping.tvdb_season = mapping_element.get("tvdbseason")
                mapping_mapping.start = mapping_element.get("start")
                mapping_mapping.end = mapping_element.get("end")
                mapping_mapping.offset = mapping_element.get("offset")
                mapping_mapping.data = mapping_element.text
                mapping.mappings.append(mapping_mapping)
        anime_mappings[mapping.aid] = mapping


def load_anime_titles():
    global anime_titles
    titles_dtg = cache.get_titles_dtg()
    if titles_dtg > 0 and titles_dtg > util.current_time() - titles_expire_days * 86400000:
        logger.debug("Getting anime titles from cache")
        raw_titles = cache.get_titles_data()
    else:
        logger.debug("Getting anime titles from web")
        raw_titles = web.get_titles()
        cache.set_titles(raw_titles)
    tree = ElementTree.ElementTree(ElementTree.fromstring(raw_titles))
    for anime_element in tree.getroot().findall("anime"):
        aid = int(anime_element.get("aid"))
        titles = []
        for title_element in anime_element.findall("title"):
            title = model.AnimeTitle()
            title.language = title_element.get("{http://www.w3.org/XML/1998/namespace}lang")
            title_type = title_element.get("type")
            if title_type == "syn":
                title_type = "synonym"
            title.type = model.AnimeTitleType[title_type]
            title.title = title_element.text
            titles.append(title)
        anime_titles[aid] = titles


def get_nice_title(aid):
    english_title = None
    main_title = None
    if aid not in anime_titles:
        return "UNKNOWN NICE TITLE FOR AID {}".format(aid)
    for title in anime_titles[aid]:
        if title.language == "en" and title.type == model.AnimeTitleType.official:
            english_title = title.title
        if title.type == model.AnimeTitleType.main:
            main_title = title.title
    if english_title is None:
        english_title = "**" + main_title
    return english_title.replace("`", "'")
