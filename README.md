# Missing Anime Relations Finder
Detects uncollected related anime based on a user's AniDB MyList.

## To Run
* Copy config-example.ini to config.ini and fill in with your AniDB info.
* Run marf.py
* Open summary.html

## Notes

If your MyList is large you will get temporarily banned from the AniDB API (marf.py will throw an exception and log an error with the message from the API). This is normal and the ban will eventually clear--wait approx. 24 hours and run marf.py again to pick up from where you left off.

Also, be VERY careful lowering either the web request rate limit or the data cache expiration limits--this could get you banned from the AniDB API.

## Output

When finished, MARF produces a summary.html file and a data dir holding raw data linked to from the summary. A cache.sqlite is also used to cache data from the web.

Inside the summary.html can be up to 6 sections:

* Not fully watched - Anime on the user's MyList which has been started but not fully watched (including specials)
* Not fully collected episodes - Anime on the user's MyList that do not have all episodes collected
* Not fully collected specials - Anime on the user's MyList that do not have all specials collected (does not yet differentiate between in-universe and not-in-universe specials)
* Missing watched relations - Anime that have not been watched by a user but are related to anime that the user has watched
* Missing relations - Anime that are related to anime on a user's MyList but are not on their MyList
* MyList dump - The user's full MyList

## Output formatting

* AniDB anime ID linking to the raw data in the data folder
* Link to the AniDB anime page
* English official title (or the main title prepended with ** if there is no official English set)
* English official title (or the main title prepended with ** if there is no official English set) from the AniDB title dump in parenthesis (only present if it differs from the anime data which can happen if the anime data is older)
* Collection data in the form A/B/C|X/Y/Z (or C|Z if not present on MyList) where:
   * A = watched collected episodes
   * B = unwatched collected episodes
   * C = total episodes in anime
   * X = watched collected specials
   * Y = unwatched collected specials
   * Z = total specials in anime
* Mapping data from Scudlee's Anime Mappings

Examples:

* 69 AniDB - One Piece - 0/299/755|0/0/23 - TVDB: 81797
* 8940 AniDB - **One Piece Film: Z (One Piece Film: Z) - 0/1/1|0/0/3 - TVDB: 81797 IMDB: tt2375379
