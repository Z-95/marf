import logging
import sqlite3
import util

logger = logging.getLogger(__name__)
user_table_creation = "CREATE TABLE IF NOT EXISTS usercache (" \
                      "user VARCHAR PRIMARY KEY NOT NULL UNIQUE, " \
                      "mylistsummary TEXT NOT NULL, " \
                      "dtg NUMERIC NOT NULL)"

anime_table_creation = "CREATE TABLE IF NOT EXISTS animecache (" \
                       "aid INTEGER PRIMARY KEY NOT NULL UNIQUE, " \
                       "data TEXT NOT NULL, " \
                       "dtg NUMERIC NOT NULL)"

titles_table_creation = "CREATE TABLE IF NOT EXISTS titlecache (" \
                        "id INTEGER PRIMARY KEY NOT NULL UNIQUE, " \
                        "data TEXT NOT NULL, " \
                        "dtg NUMERIC NOT NULL)"

mappings_table_creation = "CREATE TABLE IF NOT EXISTS mappingcache (" \
                          "id INTEGER PRIMARY KEY NOT NULL UNIQUE, " \
                          "data TEXT NOT NULL, " \
                          "dtg NUMERIC NOT NULL)"

user_data_search = "SELECT mylistsummary FROM usercache WHERE user = ?"
user_dtg_search = "SELECT dtg FROM usercache WHERE user = ?"
user_insert = "INSERT OR REPLACE INTO usercache (user,mylistsummary,dtg) VALUES (?,?,?)"
anime_data_search = "SELECT data FROM animecache WHERE aid = ?"
anime_data_dump = "SELECT aid, data FROM animecache"
anime_dtg_search = "SELECT dtg FROM animecache WHERE aid = ?"
anime_insert = "INSERT OR REPLACE INTO animecache (aid,data,dtg) VALUES (?,?,?)"
titles_data_search = "SELECT data FROM titlecache"
titles_dtg_search = "SELECT dtg FROM titlecache"
titles_insert = "INSERT OR REPLACE INTO titlecache (id,data,dtg) VALUES (1,?,?)"
mappings_data_search = "SELECT data FROM mappingcache"
mappings_dtg_search = "SELECT dtg FROM mappingcache"
mappings_insert = "INSERT OR REPLACE INTO mappingcache (id,data,dtg) VALUES (1,?,?)"
connection = sqlite3.connect("cache.sqlite")
connection.row_factory = sqlite3.Row


def close():
    logger.debug("Closing database connection")
    connection.close()


def check_tables():
    logger.debug("Checking database tables")
    with connection:
        cursor = connection.cursor()
        cursor.execute(user_table_creation)
        cursor.execute(anime_table_creation)
        cursor.execute(titles_table_creation)
        cursor.execute(mappings_table_creation)


def get_user_data(user):
    logger.debug("Getting user data for {} from cache".format(user))
    with connection:
        cursor = connection.cursor()
        cursor.execute(user_data_search, [user])
        result = cursor.fetchone()
        if result is None:
            return None
        data = result["mylistsummary"]
        return data


def get_user_dtg(user):
    logger.debug("Getting user dtg for {} from cache".format(user))
    with connection:
        cursor = connection.cursor()
        cursor.execute(user_dtg_search, [user])
        result = cursor.fetchone()
        if result is None:
            return 0
        dtg = int(result["dtg"])
        return dtg


def set_user(user, data):
    logger.debug("Setting cache data for user {}".format(user))
    with connection:
        cursor = connection.cursor()
        cursor.execute(user_insert, [user, data, util.current_time()])


def get_anime_data(aid):
    logger.debug("Getting anime data for aid {} from cache".format(aid))
    with connection:
        cursor = connection.cursor()
        cursor.execute(anime_data_search, [aid])
        result = cursor.fetchone()
        if result is None:
            return None
        data = result["data"]
        return data


def get_all_anime_data():
    logger.debug("Getting all anime data from cache")
    with connection:
        cursor = connection.cursor()
        cursor.execute(anime_data_dump)
        data = {}
        for result in cursor.fetchall():
            data[int(result["aid"])] = result["data"]
        return data


def get_anime_dtg(aid):
    logger.debug("Getting anime dtg for aid {} from cache".format(aid))
    with connection:
        cursor = connection.cursor()
        cursor.execute(anime_dtg_search, [aid])
        result = cursor.fetchone()
        if result is None:
            return 0
        dtg = int(result["dtg"])
        return dtg


def set_anime(aid, data):
    logger.debug("Setting cache data for anime {}".format(aid))
    with connection:
        cursor = connection.cursor()
        cursor.execute(anime_insert, [aid, data, util.current_time()])


def get_titles_data():
    logger.debug("Getting titles data from cache")
    with connection:
        cursor = connection.cursor()
        cursor.execute(titles_data_search)
        result = cursor.fetchone()
        if result is None:
            return None
        data = result["data"]
        return data


def get_titles_dtg():
    logger.debug("Getting titles dtg from cache")
    with connection:
        cursor = connection.cursor()
        cursor.execute(titles_dtg_search)
        result = cursor.fetchone()
        if result is None:
            return 0
        data = int(result["dtg"])
        return data


def set_titles(data):
    logger.debug("Setting cache data for titles")
    with connection:
        cursor = connection.cursor()
        cursor.execute(titles_insert, [data, util.current_time()])


def get_mappings_data():
    logger.debug("Getting mappings data from cache")
    with connection:
        cursor = connection.cursor()
        cursor.execute(mappings_data_search)
        result = cursor.fetchone()
        if result is None:
            return None
        data = result["data"]
        return data


def get_mappings_dtg():
    logger.debug("Getting mappings dtg from cache")
    with connection:
        cursor = connection.cursor()
        cursor.execute(mappings_dtg_search)
        result = cursor.fetchone()
        if result is None:
            return 0
        data = int(result["dtg"])
        return data


def set_mappings(data):
    logger.debug("Setting cache data for mappings")
    with connection:
        cursor = connection.cursor()
        cursor.execute(mappings_insert, [data, util.current_time()])
