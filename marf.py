import configparser
import logging
import os
import sys

import datasource

logging.basicConfig(stream=sys.stdout, format="%(asctime)s %(name)s %(levelname)s %(message)s", level=logging.DEBUG)
data_dump_dir = os.path.join(os.getcwd(), "data")
report_output = os.path.join(os.getcwd(), "summary.html")

logger = logging.getLogger("marf")


def main():
    config = configparser.ConfigParser()
    if not config.read("config.ini", encoding="utf-8"):
        logger.error("Could not load configuration file")
        return
    user = config.get("user", "username")
    if not user:
        logger.error("Could not load username from configuration file")
        return
    password = config.get("user", "password")
    if not password:
        logger.error("Could not load password from configuration file")
        return
    datasource.load_anime_mappings()
    datasource.load_anime_titles()
    mylist = datasource.get_mylist(user, password)
    not_fully_seen = []
    not_fully_collected_episodes = []
    not_fully_collected_specials = []
    missing_watched_relations = {}
    missing_relations = {}
    mylist_count = 1
    mylist_total = len(mylist.items)
    for aid, mylist_item in sorted(mylist.items.items()):
        logger.info("Checking mylist aid {} ({} of {})".format(aid, mylist_count, mylist_total))
        anime = datasource.get_anime(aid)
        if mylist_item.episodes_seen > 0 or mylist_item.specials_seen > 0:
            if mylist_item.episodes_seen < mylist_item.episodes or mylist_item.specials_seen < mylist_item.specials:
                not_fully_seen.append(aid)
        if mylist_item.episodes < anime.episode_count:
            not_fully_collected_episodes.append(aid)
        if mylist_item.specials < anime.special_count:
            not_fully_collected_specials.append(aid)
        scanned_missing_relations = deep_scan_relations(aid, mylist, set())
        if len(scanned_missing_relations) > 0:
            missing_relations[aid] = sorted(scanned_missing_relations)
        if mylist_item.watched:
            scanned_missing_watched_relations = deep_scan_watched_relations(aid, mylist, set())
            if len(scanned_missing_watched_relations) > 0:
                missing_watched_relations[aid] = sorted(scanned_missing_watched_relations)
        mylist_count += 1
    not_fully_seen.sort()
    not_fully_collected_episodes.sort()
    not_fully_collected_specials.sort()

    output = "<html><head>"
    output += "<style>body {font-family:sans-serif;}</style>"
    output += "<title>Missing Relations Summary for {} ({})</title>".format(user, mylist.uid)
    output += "</head><body>"
    output += "<center><h1>Missing Relations Summary for {} ({})</h1></center>".format(user, mylist.uid)
    if len(not_fully_seen) > 0:
        output += "<a href=\"#notwatched\">Not fully watched</a><br />"
    if len(not_fully_collected_episodes) > 0:
        output += "<a href=\"#notcollected\">Not fully collected episodes</a><br />"
    if len(not_fully_collected_specials) > 0:
        output += "<a href=\"#notcollectedspecials\">Not fully collected specials</a><br />"
    if len(missing_watched_relations) > 0:
        output += "<a href=\"#missingwatched\">Missing watched relations</a><br />"
    if len(missing_relations) > 0:
        output += "<a href=\"#missing\">Missing relations</a><br />"
    output += "<a href=\"#mylistdump\">Mylist dump</a><br />"
    output += "<br /><hr />"
    if len(not_fully_seen) > 0:
        output += "<a name=\"notwatched\"><h1>Not fully watched</h1></a>"
        for not_fully_seen_aid in not_fully_seen:
            output += aid_to_html(not_fully_seen_aid, mylist)
            output += "<br />"
        output += "<br /><hr />"
    if len(not_fully_collected_episodes) > 0:
        output += "<a name=\"notcollected\"><h1>Not fully collected episodes</h1></a>"
        for not_fully_collected_episodes_aid in not_fully_collected_episodes:
            output += aid_to_html(not_fully_collected_episodes_aid, mylist)
            output += "<br />"
        output += "<br /><hr />"
    if len(not_fully_collected_specials) > 0:
        output += "<a name=\"notcollectedspecials\"><h1>Not fully collected specials</h1></a>"
        for not_fully_collected_specials_aid in not_fully_collected_specials:
            output += aid_to_html(not_fully_collected_specials_aid, mylist)
            output += "<br />"
        output += "<br /><hr />"
    if len(missing_watched_relations) > 0:
        output += "<a name=\"missingwatched\"><h1>Missing watched relations</h1></a>"
        for aid, missing in sorted(missing_watched_relations.items()):
            output += aid_to_html(aid, mylist)
            output += "<br />"
            for relation_aid in missing:
                output += "-- " + aid_to_html(relation_aid, mylist)
                output += "<br />"
        output += "<br /><hr />"
    if len(missing_relations) > 0:
        output += "<a name=\"missing\"><h1>Missing relations</h1></a>"
        for aid, missing in sorted(missing_relations.items()):
            output += aid_to_html(aid, mylist)
            output += "<br />"
            for relation_aid in missing:
                output += "-- " + aid_to_html(relation_aid, mylist)
                output += "<br />"
        output += "<br /><hr />"
    output += "<a name=\"mylistdump\"><h1>Mylist dump</h1></a>"
    for aid in sorted(mylist.items.keys()):
        output += aid_to_html(aid, mylist)
        output += "<br />"

    output += "</body></html>"
    with open(report_output, "w", encoding="utf8") as output_file:
        output_file.write(output)

    datasource.dump_anime_to_dir(data_dump_dir)


def deep_scan_relations(aid, mylist, visited):
    logger.debug("Deep scanning aid {}".format(aid))
    missing = set()
    visited.add(aid)
    if aid not in mylist.items:
        missing.add(aid)
    for related_aid in datasource.get_anime(aid).related_aids:
        if related_aid not in visited:
            missing = missing.union(deep_scan_relations(related_aid, mylist, visited))
    return missing


def deep_scan_watched_relations(aid, mylist, visited):
    logger.debug("Deep watched scanning aid {}".format(aid))
    missing = set()
    visited.add(aid)
    if aid not in mylist.items or not mylist.items[aid].watched:
        missing.add(aid)
    for related_aid in datasource.get_anime(aid).related_aids:
        if related_aid not in visited:
            missing = missing.union(deep_scan_watched_relations(related_aid, mylist, visited))
    return missing


def aid_to_html(aid, mylist):
    anime = datasource.get_anime(aid)
    anime_nice_title = anime.nice_title()
    titles_nice_title = datasource.get_nice_title(aid)
    if anime_nice_title == titles_nice_title:
        title = anime_nice_title
    else:
        title = "{} ({})".format(anime_nice_title, titles_nice_title)
    output = "<a href=\"data/{}.xml\">{}</a> " \
             "<a href=https://anidb.net/perl-bin/animedb.pl?show=anime&aid={}>AniDB</a> - {}" \
        .format(aid, aid, aid, title)
    if aid in mylist.items:
        mylist_item = mylist.items[aid]
        output += " - {}/{}/{}|{}/{}/{}".format(mylist_item.episodes_seen, mylist_item.episodes, anime.episode_count,
                                                mylist_item.specials_seen, mylist_item.specials, anime.special_count)
    else:
        output += " - {}|{}".format(anime.episode_count, anime.special_count)
    if aid in datasource.anime_mappings:
        mapping_html = datasource.anime_mappings[aid].to_html()
        if mapping_html:
            output += " - {}".format(mapping_html)
    return output


if __name__ == '__main__':
    main()
